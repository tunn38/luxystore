@extends('clien.layout.master')

@section('content')
    <main class="main">
        <div class="container">
            <div class="intro-wrapper">
                <div class="row">
                    <div class="col-md-8 mb-4">
                        <div class="owl-carousel owl-theme row gutter-no cols-1 animation-slider owl-dot-inner"
                             data-owl-options="{
                                'nav': false,
                                'dots': true,
                                'items': 1,
                                'autoplay': false
                            }">
                            <div class="intro-slide intro-slide1 banner banner-fixed br-sm"
                                 style="background-image: url(images/banner2.jpg); background-color: #5D5E62;">
                                <div class="banner-content x-50 w-100 text-center">
                                    <h3 class="banner-title text-white text-uppercase slide-animate"
                                        data-animation-options="{'name': 'zoomIn', 'duration': '1s'}">An Tâm</h3>
{{--                                    <figure class="slide-animate" data-animation-options="{'name': 'fadeInDownShorter', 'duration': '1s', 'delay': '1s'}">--}}
{{--                                        <img src="assets/images/demos/demo5/slides/bicycle.png" alt="Bicycle"--}}
{{--                                             width="495" height="307" />--}}
{{--                                    </figure>--}}
                                    <p class="ls-25 slide-animate" style=" color: white; font-weight: bold" data-animation-options="{
                                            'name': 'fadeInUpShorter', 'duration': '1s', 'delay': '1s'
                                        }">Miễn Phí Giao Hàng Cho Đơn Đặt Hàng Trên <strong
                                            class="text-secondary"> 350.000đ</strong></p>
                                    <a href="shop-banner-sidebar.html"
                                       class="btn btn-white btn-link btn-underline btn-icon-right slide-animate"
                                       data-animation-options="{
                                                'name': 'fadeInUpShorter', 'duration': '1s', 'delay': '1s'
                                            }" style=" font-size: 25px; font-weight: bold">
                                        Khám Phá Ngay<i class="w-icon-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="intro-slide intro-slide2 banner banner-fixed br-sm"
                                 style="background-image: url(images/banner1.jpg); background-color: #EBEDEC;">
                                <div class="banner-content y-50">
                                    <div class="slide-animate" data-animation-options="{
                                            'name': 'fadeInRightShorter', 'duration': '1s'
                                        }">
                                        <h5 class="banner-subtitle text-uppercase text-primary ls-25">New Arrivals
                                        </h5>
                                        <h3 class="banner-title text-capitalize ls-25 mb-0">Men's Fashion</h3>
                                        <div class="banner-price-info text-default font-weight-bold mb-6 ls-50">
                                            Start at <span class="text-secondary">$12.00</span>
                                        </div>
                                        <a href="shop-banner-sidebar.html" class="btn btn-dark btn-rounded">Shop
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="intro-slide intro-slide3 banner banner-fixed br-sm"
                                 style="background-image: url(images/banner2.jpg); background-color: #E0E0DE;">
                                <div class="banner-content text-right y-50">
                                    <div class="slide-animate" data-animation-options="{
                                            'name': 'fadeInUpShorter', 'duration': '1s'
                                        }">
                                        <h5 class="banner-subtitle text-primary text-uppercase">Best Sellers</h5>
                                        <h3 class="banner-title text-capitalize lh-1 ls-25">New
                                            Sneaker<br>Collection</h3>
                                        <div class="banner-price-info text-uppercase text-default">
                                            <strong class="text-dark">Up To 10%</strong> Discount
                                        </div>
                                        <a href="shop-banner-sidebar.html" class="btn btn-dark btn-rounded">Shop
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12 col-xs-6 mb-4">
                                <div class="category-banner banner banner-fixed br-sm">
                                    <figure>
                                        <img src="images/5__61057_zoom.jpg" alt="Category"
                                             width="330" height="239" style="background-color: #00adef;" />
                                    </figure>
                                    <div class="banner-content">
                                        <h3 class="banner-title text-white text-capitalize ls-25">New
                                            Lifestyle<br>Collection</h3>
                                        <h5 class="banner-subtitle text-white text-capitalize ls-25">Discount</h5>
                                        <div class="banner-price-info text-white text-uppercase ls-25">25% Off</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-6 mb-4">
                                <div class="category-banner banner banner-fixed br-sm">
                                    <figure>
                                        <img src="images/9__00286_zoom.jpg" alt="Category"
                                             width="330" height="239" style="background-color: #eff5f5;" />
                                    </figure>
                                    <div class="banner-content">
                                        <h3 class="banner-title text-white text-capitalize ls-25 mb-3">Online
                                            Classic<br>Yoga Sale</h3>
                                        <del class="old-price text-white ls-25">350.000đ</del>
                                        <div class="new-price text-secondary ls-25">299.000đ</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Intro-wrapper -->

            <div class="owl-carousel owl-theme row cols-md-4 cols-sm-3 cols-1 icon-box-wrapper appear-animate br-sm bg-white"
                 data-owl-options="{
                    'nav': false,
                    'dots': false,
                    'loop': true,
                    'margin': 30,
                    'autoplay': true,
                    'autoplayTimeout': 4000,
                    'responsive': {
                        '0': {
                            'items': 1
                        },
                        '576': {
                            'items': 2
                        },
                        '768': {
                            'items': 2
                        },
                        '992': {
                            'items': 3
                        },
                        '1200': {
                            'items': 4
                        }
                    }
                    }">
                <div class="icon-box icon-box-side text-dark">
                        <span class="icon-box-icon icon-shipping">
                            <i class="w-icon-truck"></i>
                        </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title font-weight-bolder ls-normal">Miễn Phí Vân Chuyển</h4>
                        <p class="text-default">Cho Tất Cả Các Đơn Từ 350.000đ</p>
                    </div>
                </div>
                <div class="icon-box icon-box-side text-dark">
                    <span class="icon-box-icon icon-money">
                            <i class="w-icon-money"></i>
                        </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title font-weight-bolder ls-normal">Thanh Toán</h4>
                        <p class="text-default">Chúng Tôi Đảm Bảo Thanh Toán An Toàn</p>
                    </div>
                </div>
                <div class="icon-box icon-box-side text-dark icon-box-money">
                        <span class="icon-box-icon icon-payment">
                            <i class="w-icon-bag"></i>
                        </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title font-weight-bolder ls-normal">Chất Lượng </h4>
                        <p class="text-default">Được Kiểm Định Và Chứng Nhận</p>
                    </div>
                </div>
                <div class="icon-box icon-box-side text-dark icon-box-chat">
                        <span class="icon-box-icon icon-chat">
                            <i class="w-icon-chat"></i>
                        </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title font-weight-bolder ls-normal">Tổng Đài Hỗ Trợ</h4>
                        <p class="text-default">Hoạt Động Từ 8h00 - 22h00</p>
                    </div>
                </div>
            </div>

            <!-- End of Iocn Box Wrapper -->
            <div class="title-link-wrapper title-deals appear-animate mb-4">
                <h2 class="title title-link">Ưu Đãi Hôm Nay</h2>
                <div
                    class="product-countdown-container font-size-sm text-white bg-secondary align-items-center mr-auto">
                    <label>Áp Dụng Đến Hết Ngày 23-09-2022 </label>
                </div>
                <a href="#" class="ml-0">More Products<i class="w-icon-long-arrow-right"></i></a>
            </div>
            <div class="owl-carousel owl-theme appear-animate row cols-lg-5 cols-md-4 cols-sm-3 cols-2 mb-6"
                 data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '576': {
                            'items': 3
                        },
                        '768': {
                            'items': 4
                        },
                        '992': {
                            'items': 5
                        }
                    }
                }">
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/1-1-1.jpg" alt="Product" width="300"
                                     height="338">
                                <img src="assets/images/demos/demo5/products/1-1-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Leather Stripe Watch</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 80%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(1 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$189.00</ins><del class="old-price">$199.89</del>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/1-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Running Machine</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 60%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(1 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$27.00</ins><del class="old-price">$28.99</del>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/1-3.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Men's Black Watch</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 100%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(5 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$75.00</ins><del class="old-price">$79.00</del>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/1-4-1.jpg" alt="Product" width="300"
                                     height="338">
                                <img src="assets/images/demos/demo5/products/1-4-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Summer Sports Shoes</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 100%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(8 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$85.99</ins><del class="old-price">$88.00</del>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/1-5.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Best Pedestrian Bag</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 80%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(4 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$48.55</ins><del class="old-price">$49.00</del>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
            </div>
            <!-- End of Prodcut Deals Wrapper -->

        </div>
        <!-- End of Container -->

        <div class="container mt-10 pt-2">
            <!-- End of Banner Simple -->
            <div class="title-link-wrapper appear-animate mb-4">
                <h2 class="title title-link pt-1">Sản Phẩm Mới</h2>
                <a href="shop-boxed-banner.html">More Products<i class="w-icon-long-arrow-right"></i></a>
            </div>
            <div class="owl-carousel owl-theme products-apparel appear-animate row cols-lg-5 cols-md-4 cols-sm-3 cols-2 mb-7"
                 data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '576': {
                            'items': 3
                        },
                        '768': {
                            'items': 4
                        },
                        '992': {
                            'items': 5
                        }
                    }
                }">
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/3-1-1.jpg" alt="Product" width="300"
                                     height="338">
                                <img src="assets/images/demos/demo5/products/3-1-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Multi Function Watch</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 80%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(1 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$170.00</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/3-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Men's Suede Belt</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 60%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(1 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$39.00</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/3-3.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Gold Watch</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 100%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(5 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$210.00</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/3-4-1.jpg" alt="Product" width="300"
                                     height="338">
                                <img src="assets/images/demos/demo5/products/3-4-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Portable Charger</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 100%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(8 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$25.00</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/3-5.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Headkerchief</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 80%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(4 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$28.99</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
            </div>
            <!-- End of Prodcut Wrapper -->


            <div class="title-link-wrapper appear-animate mt-10 mb-4">
                <h2 class="title title-link pt-1">Sản Phẩm Bán Chạy</h2>
                <a href="#" class="ls-normal">More Products<i class="w-icon-long-arrow-right"></i></a>
            </div>
            <div class="owl-carousel owl-theme row appear-animate cols-lg-5 cols-md-4 cols-sm-3 cols-2 mb-9"
                 data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '576': {
                            'items': 3
                        },
                        '768': {
                            'items': 4
                        },
                        '992': {
                            'items': 5
                        }
                    }
                }">
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/4-1-1.jpg" alt="Product" width="300"
                                     height="338">
                                <img src="assets/images/demos/demo5/products/4-1-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Red Cap Sound Marker</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 80%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(1 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$65.89 - $69.99</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/4-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Men's Black Watch</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 60%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(1 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$75.00</ins><del class="old-price">$79.00</del>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/4-3.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Fabulous Sound Speaker</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 100%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(5 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$62.00</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/4-4-1.jpg" alt="Product" width="300"
                                     height="338">
                                <img src="assets/images/demos/demo5/products/4-4-2.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Mini Wireless Earphone</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 100%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(8 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$49.99</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="assets/images/demos/demo5/products/4-5.jpg" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">Good-Performance Humidifier</a>
                            </h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 80%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(4 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">$79.00</ins>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Product Wrap -->
            </div>

            <div class="title-link-wrapper appear-animate mb-4">
                <h2 class="title title-link title-blog">From Our Blog</h2>
                <a href="blog-listing.html" class="font-weight-bold font-size-normal ls-normal">View All
                    Articles</a>
            </div>
            <div class="owl-carousel owl-theme post-wrapper appear-animate row cols-lg-4 cols-md-3 cols-sm-2 cols-1 mb-3"
                 data-owl-options="{
                    'items': 4,
                    'nav': false,
                    'dots': true,
                    'loop': false,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 1
                        },
                        '576': {
                            'items': 2
                        },
                        '768': {
                            'items': 3
                        },
                        '992': {
                            'items': 4,
                            'dots': false
                        }
                    }
                }">
                <div class="post text-center overlay-zoom">
                    <figure class="post-media br-sm">
                        <a href="post-single.html">
                            <img src="assets/images/demos/demo5/blogs/1.jpg" alt="Post" width="280" height="180"
                                 style="background-color: #828896;" />
                        </a>
                    </figure>
                    <div class="post-details">
                        <div class="post-meta">
                            by <a href="#" class="post-author">John Doe</a>
                            - <a href="#" class="post-date mr-0">03.05.2021</a>
                        </div>
                        <h4 class="post-title"><a href="post-single.html">Aliquam tincidunt mauris eurisus</a></h4>
                        <a href="post-single.html" class="btn btn-link btn-dark btn-underline">Read More<i
                                class="w-icon-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="post text-center overlay-zoom">
                    <figure class="post-media br-sm">
                        <a href="post-single.html">
                            <img src="assets/images/demos/demo5/blogs/2.jpg" alt="Post" width="280" height="180"
                                 style="background-color: #C7C7C5;" />
                        </a>
                    </figure>
                    <div class="post-details">
                        <div class="post-meta">
                            by <a href="#" class="post-author">John Doe</a>
                            - <a href="#" class="post-date mr-0">03.05.2021</a>
                        </div>
                        <h4 class="post-title"><a href="post-single.html">Cras ornare tristique elit</a></h4>
                        <a href="post-single.html" class="btn btn-link btn-dark btn-underline">Read More<i
                                class="w-icon-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="post text-center overlay-zoom">
                    <figure class="post-media br-sm">
                        <a href="post-single.html">
                            <img src="assets/images/demos/demo5/blogs/3.jpg" alt="Post" width="280" height="180"
                                 style="background-color: #BDBDB5;" />
                        </a>
                    </figure>
                    <div class="post-details">
                        <div class="post-meta">
                            by <a href="#" class="post-author">John Doe</a>
                            - <a href="#" class="post-date mr-0">03.05.2021</a>
                        </div>
                        <h4 class="post-title"><a href="post-single.html">Vivamus vestibulum ntulla nec ante</a>
                        </h4>
                        <a href="post-single.html" class="btn btn-link btn-dark btn-underline">Read More<i
                                class="w-icon-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="post text-center overlay-zoom">
                    <figure class="post-media br-sm">
                        <a href="post-single.html">
                            <img src="assets/images/demos/demo5/blogs/4.jpg" alt="Post" width="280" height="180"
                                 style="background-color: #546B73;" />
                        </a>
                    </figure>
                    <div class="post-details">
                        <div class="post-meta">
                            by <a href="#" class="post-author">John Doe</a>
                            - <a href="#" class="post-date mr-0">03.05.2021</a>
                        </div>
                        <h4 class="post-title"><a href="post-single.html">Fusce lacinia arcuet nulla</a></h4>
                        <a href="post-single.html" class="btn btn-link btn-dark btn-underline">Read More<i
                                class="w-icon-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
