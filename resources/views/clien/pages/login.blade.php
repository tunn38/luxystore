 <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <title>Wolmart - Bootstrap eCommerce Template</title>

        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Wolmart - Bootstrap eCommerce Template">
        <meta name="author" content="D-THEMES">

        <!-- Favicon -->
        <link rel="icon" type="image/png" href="assets/images/icons/favicon.png">

        <!-- WebFont.js -->
        <script>
            WebFontConfig = {
                google: { families: ['Poppins:400,500,600,700,800'] }
            };
            (function (d) {
                var wf = d.createElement('script'), s = d.scripts[0];
                wf.src = 'assets/js/webfont.js';
                wf.async = true;
                s.parentNode.insertBefore(wf, s);
            })(document);
        </script>
        <script src="bootstrap-4.6.2-dist/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="bootstrap-4.6.2-dist/css/bootstrap.min.css" type="text/css">
        <link rel="preload" href="assets/vendor/fontawesome-free/webfonts/fa-regular-400.woff2" as="font" type="font/woff2"
              crossorigin="anonymous">
        <link rel="preload" href="assets/vendor/fontawesome-free/webfonts/fa-solid-900.woff2" as="font" type="font/woff2"
              crossorigin="anonymous">
        <link rel="preload" href="assets/vendor/fontawesome-free/webfonts/fa-brands-400.woff2" as="font" type="font/woff2"
              crossorigin="anonymous">
        <link rel="preload" href="assets/fonts/wolmart87d5.ttf?png09e" as="font" type="font/ttf" crossorigin="anonymous">

        <!-- Vendor CSS -->
        <link rel="stylesheet" type="text/css" href="assets/vendor/fontawesome-free/css/all.min.css">

        <!-- Plugins CSS -->
        <link rel="stylesheet" type="text/css" href="assets/vendor/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="assets/vendor/animate/animate.min.css">
        <link rel="stylesheet" type="text/css" href="assets/vendor/magnific-popup/magnific-popup.min.css">

        <!-- Default CSS -->
        <link rel="stylesheet" type="text/css" href="assets/css/demo5.min.css">
    </head>

    <body class="home">
    <!-- Start of Main -->
    <main class="main login-page">
        <!-- End of Breadcrumb -->
        <div class="page-content">
                <hr class="mt-0 mb-0">
                <nav class="breadcrumb-nav mb-2 mt-2">
                    <div class="container">
                        <ul class="breadcrumb d-flex">
                            <li><a style="color: #0A4BFA" href="home">Trang Chủ </a></li>
                            <li class="ml-2 mr-2"><i style="width: 20px; height: 20px" class="w-icon-angle-right"></i></li>
                            <li>Login</li>
                        </ul>
                    </div>
                </nav>
                <hr class="mb-0">
            <div class="container">
                <div class="login-popup " style="margin: auto;">
                    <div class="tab tab-nav-boxed tab-nav-center tab-nav-underline">
                        <ul class="nav nav-tabs text-uppercase" role="tablist">
                            <li class="nav-item">
                                <a href="#sign-in" class="nav-link active">Đăng Nhập</a>
                            </li>
                            <li class="nav-item">
                                <a href="#sign-up" class="nav-link">Đăng Ký</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="sign-in">
                                <div class="form-group">
                                    <label>Tên Người Dùng Hoắc Số Điện Thoại <b style="color: red">*</b></label>
                                    <input type="text" class="form-control" name="username" id="username" required>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Mật Khẩu <b style="color: red">*</b></label>
                                    <input type="text" class="form-control" name="password" id="password" required>
                                </div>
                                <div class="form-checkbox d-flex align-items-center justify-content-between">
                                    <input type="checkbox" class="custom-checkbox" id="remember1" name="remember1" required="">
                                    <label for="remember1">Lưu Đăng Nhập</label>
                                    <a href="#">Quên Mật Khẩu?</a>
                                </div>
                                <a href="#" class="btn btn-primary">Đăng Nhập</a>
                            </div>
                            <div class="tab-pane" id="sign-up">
                                <div class="form-group">
                                    <label>Số Điện Thoại <b style="color: red">*</b></label>
                                    <input type="text" class="form-control" name="phone" id="phone" required>
                                </div>
                                <div class="checkbox-content login-vendor">
                                    <div class="form-group mb-5">
                                        <label>Họ Và Tên <b style="color: red">*</b></label>
                                        <input type="text" class="form-control" name="name" id="name" required>
                                    </div>
                                </div>
                                <div class="checkbox-content login-vendor">
                                    <div class="form-group mb-5">
                                        <label>Địa Chỉ <b style="color: red">*</b></label>
                                        <input type="text" class="form-control" name="address" id="address" required>
                                    </div>
                                </div>
                                <div class="form-group mb-5">
                                    <label>Mật Khẩu <b style="color: red">*</b></label>
                                    <input type="text" class="form-control" name="password_1" id="password_1" required>
                                </div>
                                <div class="form-group mb-5">
                                    <label>Xác Nhận Mật Khẩu <b style="color: red">*</b></label>
                                    <input type="text" class="form-control" name="password_1" id="password_1" required>
                                </div>
                                <div class="form-checkbox user-checkbox mt-0">
                                    <input type="checkbox" class="custom-checkbox checkbox-round active" id="check-customer" name="check-customer" required="">
                                    <label for="check-customer" class="check-customer mb-1">Xác Nhận Đăng Ký</label>
                                </div>
                                <a href="#" class="btn btn-primary">Đăng Kí</a>
                            </div>
                        </div>
                        <p class="text-center">Đăng Nhập Bằng Mạng Xã Hội</p>
                        <div class="social-icons social-icon-border-color d-flex justify-content-center">
                            <a href="#" class="social-icon social-facebook w-icon-facebook"></a>
                            <a href="#" class="social-icon social-google fab fa-google"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- Plugin JS File -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/jquery.plugin/jquery.plugin.min.js"></script>
    <script src="assets/vendor/parallax/parallax.min.js"></script>
    <script src="assets/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="assets/vendor/jquery.countdown/jquery.countdown.min.js"></script>
    <script src="assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendor/zoom/jquery.zoom.js"></script>
    <script src="assets/vendor/skrollr/skrollr.min.js"></script>

    <script src="assets/js/main.min.js"></script>
    </body>
