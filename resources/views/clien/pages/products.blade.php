@extends('clien.layout.master')

@section('content')
    <div class="container mt-10 pt-2">
        <!-- End of Banner Simple -->
        <div class="title-link-wrapper appear-animate mb-4">
            <h2 class="title title-link pt-1">Sản Phẩm Mới</h2>
            <a href="shop-boxed-banner.html">More Products<i class="w-icon-long-arrow-right"></i></a>
        </div>
        <div class="owl-carousel owl-theme products-apparel appear-animate row cols-lg-5 cols-md-4 cols-sm-3 cols-2 mb-7"
             data-owl-options="{'nav': false,'dots': true,'margin': 20,'responsive': {'0': {'items': 2},'576': {'items': 3},'768': {'items': 4},'992': {'items': 5}}}">
            @foreach($new_products as $product)
            <div class="product-wrap">
                <div class="product text-center">
                    <figure class="product-media">
                        <a href="product-default.html">
                            <img src="{{ url("storage/products/".$product['image_url']) }}" alt="Product" width="300"
                                 height="338">
                            <img src="{{ url("storage/products/".$product['image_url']) }}" alt="Product" width="300"
                                 height="338">
                        </a>
                        <div class="product-action-vertical">
                            <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                            <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                               title="Add to wishlist"></a>
                            <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                               title="Quickview"></a>
                            <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                               title="Add to Compare"></a>
                        </div>
                    </figure>
                    <div class="product-details">
                        <h4 class="product-name"><a href="product-default.html">{{ $product['name'] }}</a></h4>
                        <div class="ratings-container">
                            <div class="ratings-full">
                                <span class="ratings" style="width: 80%;"></span>
                                <span class="tooltiptext tooltip-top"></span>
                            </div>
                            <a href="product-default.html" class="rating-reviews">(1 Reviews)</a>
                        </div>
                        <div class="product-price">
                            <ins class="new-price">{{ $product['price'] }}</ins>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <!-- End of Prodcut Wrapper -->


        <div class="title-link-wrapper appear-animate mt-10 mb-4">
            <h2 class="title title-link pt-1">Sản Phẩm Bán Chạy</h2>
            <a href="#" class="ls-normal">More Products<i class="w-icon-long-arrow-right"></i></a>
        </div>
        <div class="owl-carousel owl-theme products-apparel appear-animate row cols-lg-5 cols-md-4 cols-sm-3 cols-2 mb-7"
             data-owl-options="{'nav': false,'dots': true,'margin': 20,'responsive': {'0': {'items': 2},'576': {'items': 3},'768': {'items': 4},'992': {'items': 5}}}">
            @foreach($new_products as $product)
                <div class="product-wrap">
                    <div class="product text-center">
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="{{ url("storage/products/".$product['image_url']) }}" alt="Product" width="300"
                                     height="338">
                                <img src="{{ url("storage/products/".$product['image_url']) }}" alt="Product" width="300"
                                     height="338">
                            </a>
                            <div class="product-action-vertical">
                                <a href="#" class="btn-product-icon btn-cart w-icon-cart" title="Add to cart"></a>
                                <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"
                                   title="Add to wishlist"></a>
                                <a href="#" class="btn-product-icon btn-quickview w-icon-search"
                                   title="Quickview"></a>
                                <a href="#" class="btn-product-icon btn-compare w-icon-compare"
                                   title="Add to Compare"></a>
                            </div>
                        </figure>
                        <div class="product-details">
                            <h4 class="product-name"><a href="product-default.html">{{ $product['name'] }}</a></h4>
                            <div class="ratings-container">
                                <div class="ratings-full">
                                    <span class="ratings" style="width: 80%;"></span>
                                    <span class="tooltiptext tooltip-top"></span>
                                </div>
                                <a href="product-default.html" class="rating-reviews">(1 Reviews)</a>
                            </div>
                            <div class="product-price">
                                <ins class="new-price">{{ $product['price'] }}</ins>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
@endsection
