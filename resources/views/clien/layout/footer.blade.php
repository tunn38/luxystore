<footer class="footer appear-animate" data-animation-options="{'name': 'fadeIn'}">
    <div class="" style="padding: 0 10px">
        <div class="footer-top">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <div class="widget widget-about">
                        <div class="widget-body">
                            <div class="d-block"><img style="display: block;margin: auto" src="images/logo.jpg" alt="logo" width="145" height="45" /><h2 style=" text-align: center ">Nhà Thuốc An Tâm</h2></div>
                            <p style=" text-align: center " class="widget-about-title">Liên Hệ Với Chúng Tôi 24/7</p>
                            <a style=" text-align: center " href="tel:0969647473" class="widget-about-call">+84 969-647-473</a>
                            <label style=" text-align: center " class="label-social d-block text-dark">Mạng Xã Hội</label>
                            <div style=" justify-content: center ; width: 100%" class="social-icons social-icons-colored">
                                <a href="#" class="social-icon social-facebook w-icon-facebook"></a>
                                <a href="#" class="social-icon social-twitter w-icon-twitter"></a>
                                <a href="#" class="social-icon social-instagram w-icon-instagram"></a>
                                <a href="#" class="social-icon social-youtube w-icon-youtube"></a>
                                <a href="#" class="social-icon social-pinterest w-icon-pinterest"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="widget">
                        <h3 class="widget-title">VỀ CHÚNG TÔI</h3>
                        <ul class="widget-body">
                            <li><a href="#">Giới thiệu</a></li>
                            <li><a href="#">Hệ thống cửa hàng</a></li>
                            <li><a href="#">Giấy phép kinh doanh</a></li>
                            <li><a href="#">Quy chế hoạt động</a></li>
                            <li><a href="#">Chính sách đặt cọc</a></li>
                            <li><a href="#">Chính sách đổi trả thuốc</a></li>
                            <li><a href="#">Chính sách giao hàng</a></li>
                            <li><a href="#">Chính sách bảo mật</a></li>
                            <li><a href="#">Chính sách thanh toán</a></li>
                            <li><a href="#">Kiểm tra hóa đơn điện tử</a></li>
                            <li><a href="#">Tra cứu đơn hàng</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="widget">
                        <h4 class="widget-title">DANH MỤC</h4>
                        <ul class="widget-body">
                            <li><a href="#">Thực phẩm chức năng</a></li>
                            <li><a href="#">Dược mỹ phẩm</a></li>
                            <li><a href="#">Chăm sóc cá nhân</a></li>
                            <li><a href="#">Thuốc</a></li>
                            <li><a href="#">Trang thiết bị y tế</a></li>
                            <li><a href="#">Bệnh</a></li>
                            <li><a href="#">Góc sức khoẻ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="widget">
                        <h4 class="widget-title">THỰC PHẨM CHỨC NĂNG</h4>
                        <ul class="widget-body">
                            <li><a href="#">Sinh lý - Nội tiết tố</a></li>
                            <li><a href="#">Sức khỏe tim mạch</a></li>
                            <li><a href="#">Hỗ trợ điều trị</a></li>
                            <li><a href="#">Thần kinh não</a></li>
                            <li><a href="#">Hỗ trợ tiêu hóa</a></li>
                            <li><a href="#">Cải thiện tăng cường chức năng</a></li>
                            <li><a href="#">Hỗ trợ làm đẹp</a></li>
                            <li><a href="#">Vitamin & Khoáng chất</a></li>
                            <li><a href="#">Dinh dưỡng</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-left">
                <p class="copyright">Copyright © 2021 Wolmart Store. All Rights Reserved.</p>
            </div>
            <div class="footer-right">
                <span class="payment-label mr-lg-8">We're using safe payment for</span>
                <figure class="payment">
                    <img src="assets/images/payment.png" alt="payment" width="159" height="25" />
                </figure>
            </div>
        </div>
    </div>
</footer>
