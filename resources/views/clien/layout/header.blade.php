<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="header-left">
                <p class="welcome-msg">CHÀO MỪNG BẠN ĐẾN VỚI NHÀ THUỐC CHÚNG TÔI !</p>
            </div>
            <div class="header-right">
                <div class="dropdown">
                </div>
                <div class="dropdown">
                </div>
                <!-- End of Dropdown Menu -->
                <span class="divider d-lg-show"></span>
                <a href="blog.html" class="d-lg-show">Blog</a>
                <a href="contact-us.html" class="d-lg-show">Liên Hệ</a>
                <a href="my-account.html" class="d-lg-show">Tài Khoản</a>
                <a href="assets/ajax/login.html" class="d-lg-show login sign-in"><i
                        class="w-icon-account"></i>Đăng Nhập</a>
                <span class="delimiter d-lg-show">/</span>
                <a href="assets/ajax/login.html" class="ml-0 d-lg-show login register">Đăng Ký</a>
            </div>
        </div>
    </div>
    <!-- End of Header Top -->

    <div class="header-middle ">
        <div class="container">
            <div class="header-left mr-md-4">
                <a href="#" class="mobile-menu-toggle  w-icon-hamburger">
                </a>
                <a href="demo5.html" class="logo ml-lg-0">
                    <img src="images/logo.jpg" alt="logo" width="145" height="45" />
                </a>
                <form method="get" action="#" class="header-search hs-expanded hs-round d-none d-md-flex input-wrapper">
                    <div class="select-box">
                        <select id="category" name="category">
                            <option value="">Tìm Kiếm Tất Cả</option>
                            <option value="4">Sức Khỏe</option>
                            <option value="5">Sắc Đẹp</option>
                            <option value="5">Chăm Sóc Cá Nhân</option>
                            <option value="6">Mỹ Phẩm</option>
                            <option value="7">Thức Phẩm Chức Năng</option>
                            <option value="8">Thuốc Đặc Trị</option>
                            <option value="9">Thiết Bị Dụng Cụ Y Tế</option>
                        </select>
                    </div>
                    <input type="text" class="form-control" name="search" id="search"
                           placeholder="Nhập Từ Khóa ..." required />
                    <button class="btn btn-search" type="submit"><i class="w-icon-search"></i>
                    </button>
                </form>
            </div>
            <div class="header-right ml-4">
                <div class="header-call d-xs-show d-lg-flex align-items-center">
                    <a href="tel:#" class="w-icon-call"></a>
                    <div class="call-info d-lg-show">
                        <h4 class="chat font-weight-normal font-size-md text-normal ls-normal text-light mb-0">
                            <a href="mailto:#" class="text-capitalize">LIÊN HỆ NGAY</a> :</h4>
                        <a href="tel:#" class="phone-number font-weight-bolder ls-50">0969647473</a>
                    </div>
                </div>
                <a class="wishlist label-down link d-xs-show" href="wishlist.html">
                    <i class="w-icon-heart"></i>
                    <span class="wishlist-label d-lg-show">Yêu Thích</span>
                </a>
                <div class="dropdown cart-dropdown cart-offcanvas mr-0 mr-lg-2">
                    <div class="cart-overlay"></div>
                    <a href="#" class="cart-toggle label-down link">
                        <i class="w-icon-cart">
                            <span class="cart-count">2</span>
                        </i>
                        <span class="cart-label">Giỏ Hàng</span>
                    </a>
                    <div class="dropdown-box">
                        <div class="cart-header">
                            <span>Shopping Cart</span>
                            <a href="#" class="btn-close">Close<i class="w-icon-long-arrow-right"></i></a>
                        </div>

                        <div class="products">
                            <div class="product product-cart">
                                <div class="product-detail">
                                    <a href="product-default.html" class="product-name">Beige knitted
                                        elas<br>tic
                                        runner shoes</a>
                                    <div class="price-box">
                                        <span class="product-quantity">1</span>
                                        <span class="product-price">$25.68</span>
                                    </div>
                                </div>
                                <figure class="product-media">
                                    <a href="product-default.html">
                                        <img src="assets/images/cart/product-1.jpg" alt="product" height="84"
                                             width="94" />
                                    </a>
                                </figure>
                                <button class="btn btn-link btn-close">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>

                            <div class="product product-cart">
                                <div class="product-detail">
                                    <a href="product-default.html" class="product-name">Blue utility
                                        pina<br>fore
                                        denim dress</a>
                                    <div class="price-box">
                                        <span class="product-quantity">1</span>
                                        <span class="product-price">$32.99</span>
                                    </div>
                                </div>
                                <figure class="product-media">
                                    <a href="product-default.html">
                                        <img src="assets/images/cart/product-2.jpg" alt="product" width="84"
                                             height="94" />
                                    </a>
                                </figure>
                                <button class="btn btn-link btn-close">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div class="cart-total">
                            <label>Subtotal:</label>
                            <span class="price">$58.67</span>
                        </div>

                        <div class="cart-action">
                            <a href="cart.html" class="btn btn-dark btn-outline btn-rounded">View Cart</a>
                            <a href="checkout.html" class="btn btn-primary  btn-rounded">Checkout</a>
                        </div>
                    </div>
                    <!-- End of Dropdown Box -->
                </div>
            </div>
        </div>
    </div>
    <!-- End of Header Middle -->

    <div class="header-bottom sticky-content fix-top sticky-header has-dropdown">
        <div class="container">
            <div class="inner-wrap">
                <div class="header-left">
                    <div class="dropdown category-dropdown show-dropdown" data-visible="true">
                        <a href="#" class="text-white category-toggle" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="true" data-display="static"
                           title="Browse Categories">
                            <i class="w-icon-category"></i>
                            <span>Tất cả Danh Mục</span>
                        </a>

                        <div class="dropdown-box h-100" >
                            <ul class="menu vertical-menu category-menu h-100">
                                <li class="h-25"><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Sức Khỏe
                                    </a></li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Sắc Đẹp
                                    </a></li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Chăm Sóc Cá Nhân
                                    </a></li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Mỹ Phẩm
                                    </a></li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Thực Phẩm Chức Năng
                                    </a></li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Thuốc Đặc Trị
                                    </a></li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Dụng CỤ Y Tế
                                    </a></li>
                                <li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Dụng CỤ Y Tế
                                    </a></li>
                                <li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Dụng CỤ Y Tế
                                    </a></li>
                                <li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Dụng CỤ Y Tế
                                    </a></li>
                                <li>
                                <li><a href="shop-fullwidth-banner.html">
                                        <i class="w-icon-ruby"></i>Dụng CỤ Y Tế
                                    </a></li>
                                <li>
                                    <a href="shop-banner-sidebar.html"
                                       class="font-weight-bold text-uppercase ls-25">
                                        Tất Cả Sản Phẩm<i class="w-icon-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <nav class="main-nav">
                        <ul class="menu active-underline">
                            <li class="active">
                                <a href="home">Trang Chủ</a>
                            </li>
                            <li>
                                <a href="products">Sản Phẩm</a>

                                <!-- Start of Megamenu -->
                                <ul class="megamenu">
                                    <li>
                                        <h4 class="menu-title">Shop Pages</h4>
                                        <ul>
                                            <li><a href="shop-banner-sidebar.html">Banner With Sidebar</a></li>
                                            <li><a href="shop-boxed-banner.html">Boxed Banner</a></li>
                                            <li><a href="shop-fullwidth-banner.html">Full Width Banner</a></li>
                                            <li><a href="shop-horizontal-filter.html">Horizontal Filter<span
                                                        class="tip tip-hot">Hot</span></a></li>
                                            <li><a href="shop-off-canvas.html">Off Canvas Sidebar<span
                                                        class="tip tip-new">New</span></a></li>
                                            <li><a href="shop-infinite-scroll.html">Infinite Ajax Scroll</a>
                                            </li>
                                            <li><a href="shop-right-sidebar.html">Right Sidebar</a></li>
                                            <li><a href="shop-both-sidebar.html">Both Sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <h4 class="menu-title">Shop Layouts</h4>
                                        <ul>
                                            <li><a href="shop-grid-3cols.html">3 Columns Mode</a></li>
                                            <li><a href="shop-grid-4cols.html">4 Columns Mode</a></li>
                                            <li><a href="shop-grid-5cols.html">5 Columns Mode</a></li>
                                            <li><a href="shop-grid-6cols.html">6 Columns Mode</a></li>
                                            <li><a href="shop-grid-7cols.html">7 Columns Mode</a></li>
                                            <li><a href="shop-grid-8cols.html">8 Columns Mode</a></li>
                                            <li><a href="shop-list.html">List Mode</a></li>
                                            <li><a href="shop-list-sidebar.html">List Mode With Sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <h4 class="menu-title">Product Pages</h4>
                                        <ul>
                                            <li><a href="product-variable.html">Variable Product</a></li>
                                            <li><a href="product-featured.html">Featured &amp; Sale</a></li>
                                            <li><a href="product-accordion.html">Data In Accordion</a></li>
                                            <li><a href="product-section.html">Data In Sections</a></li>
                                            <li><a href="product-swatch.html">Image Swatch</a></li>
                                            <li><a href="product-extended.html">Extended Info</a>
                                            </li>
                                            <li><a href="product-without-sidebar.html">Without Sidebar</a></li>
                                            <li><a href="product-video.html">360<sup>o</sup> &amp; Video<span
                                                        class="tip tip-new">New</span></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <h4 class="menu-title">Product Layouts</h4>
                                        <ul>
                                            <li><a href="product-default.html">Default<span
                                                        class="tip tip-hot">Hot</span></a></li>
                                            <li><a href="product-vertical.html">Vertical Thumbs</a></li>
                                            <li><a href="product-grid.html">Grid Images</a></li>
                                            <li><a href="product-masonry.html">Masonry</a></li>
                                            <li><a href="product-gallery.html">Gallery</a></li>
                                            <li><a href="product-sticky-info.html">Sticky Info</a></li>
                                            <li><a href="product-sticky-thumb.html">Sticky Thumbs</a></li>
                                            <li><a href="product-sticky-both.html">Sticky Both</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- End of Megamenu -->
                            </li>
                            <li>
                                <a href="blog">Blog</a>
                                <ul>
                                    <li><a href="blog.html">Classic</a></li>
                                    <li><a href="blog-listing.html">Listing</a></li>
                                    <li>
                                        <a href="blog-grid-3cols.html">Grid</a>
                                        <ul>
                                            <li><a href="blog-grid-2cols.html">Grid 2 columns</a></li>
                                            <li><a href="blog-grid-3cols.html">Grid 3 columns</a></li>
                                            <li><a href="blog-grid-4cols.html">Grid 4 columns</a></li>
                                            <li><a href="blog-grid-sidebar.html">Grid sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="blog-masonry-3cols.html">Masonry</a>
                                        <ul>
                                            <li><a href="blog-masonry-2cols.html">Masonry 2 columns</a></li>
                                            <li><a href="blog-masonry-3cols.html">Masonry 3 columns</a></li>
                                            <li><a href="blog-masonry-4cols.html">Masonry 4 columns</a></li>
                                            <li><a href="blog-masonry-sidebar.html">Masonry sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="blog-mask-grid.html">Mask</a>
                                        <ul>
                                            <li><a href="blog-mask-grid.html">Blog mask grid</a></li>
                                            <li><a href="blog-mask-masonry.html">Blog mask masonry</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="post-single.html">Single Post</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="">
                                <a href="introduce">Giới Thiệu</a>
                            </li>
                            <li class="">
                                <a href="demo5.html">Danh Hiệu</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="header-right">
                    <a href="#" class="d-xl-show"><i class="w-icon-map-marker mr-1"></i>Địa chỉ</a>
                    <a href="#"><i class="w-icon-sale"></i>Giảm Giá</a>
                </div>
            </div>
        </div>
    </div>
    <div>
    <hr class="mt-0 mb-0">
    <nav class="breadcrumb-nav mb-2 mt-2">
        <div class="container">
            <ul class="breadcrumb d-flex">
                <li><a style="color: #0A4BFA" href="home">Trang Chủ </a></li>
                <li class="ml-2 mr-2"><i style="width: 20px; height: 20px" class="w-icon-angle-right"></i></li>
                <li>xxx</li>
            </ul>
        </div>
    </nav>
    <hr>
    </div>
</header>

