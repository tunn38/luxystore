<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <style>
        .td-left {
            background: #CCCCCC;
            border: 1px solid;
            padding: 4px;
            text-align: center;
            width: 25%;
        }
        .input-form {
            border: 1px solid;
            width: 75%;
            margin-right: auto ;
        }
        .input-form .input-text{
            width: 100%;
            margin: 2px 0;
        }
        .input-form textarea{
            width: 80%;
            height: 80px;
            margin: 2px;
        }
        .input-form img{
            width: auto;
            height: 150px;
            margin: 2px;
        }
        .button-form {
            border-bottom: 1px solid;
            border-right: 1px solid;
            border-top: 1px solid;
        }
        .header-form {
            border-right: 1px solid;
            border-top: 1px solid;
            border-left: 1px solid;
            background: #cccccc;
        }

        .button {
            width: 48%;
            height: 35px;
            align-content: center;
            margin: auto;
            border-radius: 10px;
            border: none;
            font-weight: bold;
            font-size: 20px;
            color: white;
        }

        .background-5A5959 {
            background: #5A5959;
            box-shadow: 0 4px #101010;
        }

        .background-EB6100 {
            background: #EB6100;
            box-shadow: 0 4px #B74C00;
        }

        .text-validate {
            color: red;
            margin-left: 3px;
            font-weight: bold;
        }


    </style>
</head>
<body>
<div class="d-block ml-4">
    <div class="mt-3 mb-1">
        <button type="button" class="font-weight-bold ">戻る</button>
    </div>
    <div class="mt-2 mb-1">
        <p class="font-weight-bold">xxxxxxx</p>
    </div>
    <div class="border" style=" max-width: 70%">
        <div class="mb-0 header-form d-flex justify-content-center">
            <p class="mb-0 p-1 ">アイテム設定(アイテムID「１」更新中)</p>
        </div>
        <div class="d-flex">
            <div class="w-75">
                <table class="w-100">
                    <tr class="w-100">
                        <td class="td-left">アイテムID</td>
                        <td class="input-form">1</td>
                    </tr>
                    <tr class="w-100">
                        <td class="td-left"><div class="d-flex justify-content-center">アイテム名 <p class="mb-0 text-validate">*</p></div></td>
                        <td class="input-form"><input class="input-text" type="text"><div><p class="text-validate mb-0 d-none">ccccccccccccc</p>
                            </div>
                        </td>
                    </tr>
                    <tr class="w-100">
                        <td class="td-left">アイテム説明</td>
                        <td class="input-form"><textarea></textarea><div><p class="text-validate mb-0 d-none">ccccccccccccc</p>
                            </div>
                        </td>
                    </tr>
                    <tr class="w-100">
                        <td class="td-left">Anroid URL</td>
                        <td class="input-form"><input class="input-text" type="text"><div><p class="text-validate mb-0 d-none">ccccccccccccc</p>
                            </div>
                        </td>
                    </tr>
                    <tr class="w-100">
                        <td class="td-left">iOS URL</td>
                        <td class="input-form"><input class="input-text" type="text"><div><p class="text-validate mb-0 d-none d-none">ccccccccccccc</p>
                            </div>
                        </td>
                    </tr>
                    <tr class="w-100">
                        <td class="td-left"><div class="d-flex justify-content-center">ファイルを選択 <p class="mb-0 text-validate">*</p></div></td>
                        <td class="input-form"><input type="file" id="imgInp"><br><img id="img" src="#" alt="your image" class="d-none"/>
                            <div><p class="text-validate mb-0 d-none">ccccccccccccc</p></div>
                        </td>
                    </tr>
                    <tr class="w-100">
                        <td class="td-left">状態</td>
                        <td class="input-form">
                            <div class="d-flex">
                                <input type="radio" name="xxx" value="" class="mx-1"><lablr>有効</lablr>
                                <input type="radio" name="xxx" value="" class="ml-3 mr-1"><lablr>無効</lablr>
                            </div>
                            <div><p class="text-validate mb-0 d-none">ccccccccccccc</p></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="button-form justify-content-center d-flex w-25">
                <button class="button background-5A5959" >更新</button>
                <button class="button background-EB6100" >登録</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
<script>
    imgInp.onchange = evt => {
        const [file] = imgInp.files
        if (file) {
            img.src = URL.createObjectURL(file);
            var element = document.getElementById("img");
            element.classList.remove("d-none");
        }
    }
</script>
