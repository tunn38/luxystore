@extends('admin.layout.master')

@section('content')
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="d-flex">
                <h4 class="card-title">Thêm Sản Phẩm </h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <button class="btn btn-secondary text-white mb-2" onclick="location.href='../product'"> Trở
                                Về
                            </button>
                        </div>
                        <div class="col-lg-8 col-xlg-9 col-md-7">
                            <div class="card">
                                <div class="card-body">
                                    <h4 id="errror_all" class="d-none text-center" style="color: red">Chưa Nhập Đủ Thông
                                        Tin !</h4>
                                    <form name="product" class="form-horizontal form-material mx-2" name="product" method="post"
                                          onsubmit="return validateForm()" action="{{ route("create_product") }}"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label class="col-sm-12">Danh Mục Cha</label>
                                            <div class="col-sm-12">
                                                <select name="categories_id" onchange="FetchCategories(this.value)"
                                                        class="form-select shadow-none form-control-line">
                                                    @foreach($category_dad as $category)
                                                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                                    @endforeach
                                                </select>
                                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">Danh Mục Con</label>
                                            <div class="col-sm-12">
                                                <select name="category_dad_id" onchange="FetchCategories(this.value)"
                                                        class="form-select shadow-none form-control-line">
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                                    @endforeach
                                                </select>
                                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">Loại Sản Phẩm</label>
                                            <div class="col-sm-12">
                                                <select name="product_type_id" id="product_type_id"
                                                        class="form-select shadow-none form-control-line">
                                                    @foreach($product_type as $type)
                                                        <option value="{{ $type['id'] }}">{{ $type['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Tên Sản Phẩm</label>
                                            <div class="col-md-12">
                                                <input type="text" name="name" placeholder="Nhập Tên..."
                                                       class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Giá Sản Phẩm</label>
                                            <div class="col-md-12">
                                                <input type="text" name="price" placeholder="Nhập Giá..."
                                                       class="form-control form-control-line">
                                            </div>
                                            <p id="errror_price" class="d-none" style="color: red">Giá Chưa Hợp Lệ !</p>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Thông Tin Sản Phẩm</label>
                                            <div class="col-md-12">
                                                <textarea name="description" placeholder="Nhập Giá..."
                                                          class="form-control form-control-line" disabled></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Ảnh Sản Phẩm</label>
                                            <div class="col-md-12">
                                                <input type="file" name="image" class="form-control form-control-line"
                                                       id="imgInp">
                                                <br><img id="img" src="#" alt="your image" class="d-none"
                                                         style="width: auto;height: 150px;margin: 2px;"/>
                                            </div>
                                            <p id="errror_img" class="d-none" style="color: red">File Không Hợp Lệ !</p>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-success text-white mb-5">
                                                    Cập Nhật
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function validateForm() {
            var errror_all = document.getElementById("errror_all");
            var error_price = document.getElementById("errror_price");
            var error_img = document.getElementById("error_img");
            errror_all.classList.add("d-none")
            error_price.classList.add("d-none")
            // error_img.classList.add("d-none")

            let name = document.forms["product"]["name"].value;
            let price = document.forms["product"]["price"].value;
            let description = document.forms["product"]["description"].value;
            let image = document.forms["product"]["image"].value;
            let product_type_id = document.forms["product"]["product_type_id"].value;
            if (name === "" || price === "" || description === "" || image === "" || product_type_id === "") {
                errror_all.classList.remove("d-none")
                return false;
            }
            if (isNaN(price)) {
                errror_all.classList.add("d-none")
                error_price.classList.remove("d-none")
                return false;
            }
        }

        imgInp.onchange = evt => {
            const [file] = imgInp.files
            if (file) {
                img.src = URL.createObjectURL(file);
                var element = document.getElementById("img");
                element.classList.remove("d-none");
            }
        }

        function FetchCategories(id) {

            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            // });
            // $.ajax({
            //     url: '../product/create',
            //     type:'post',
            //     data: {categories : id},
            //     success: function(data) {
            //         alert(data);
            //     },
            //     error: function(data){
            //         alert('failse');
            //     }
            // })
        }
    </script>
@endsection
