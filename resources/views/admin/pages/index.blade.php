@extends('admin.layout.master')

@section('header')
    <div class="d-flex">
        <h4 class="card-title">Loại Sản Phẩm </h4>
    </div>
@endsection
@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div>
                    <button class="btn btn-success text-white mb-2" onclick="">+ Tạo Mới </button>
                </div>
                <h4 class="card-title">Danh Sách : </h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">NO</th>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Xóa Bỏ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>12</td>
                            <td>Thực Phẩm Chức Năng</td>
                            <td><button class="btn btn-danger text-white">Xóa</button></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
