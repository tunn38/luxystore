<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <style>
        table tr th {
            background: #CCCCCC;
            border: 1px solid;
            padding: 4px;
            text-align: center;
        }
        table tr td {
            border: 1px solid;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="d-block ml-4">
    <div class="mt-3 mb-1">
        <a href="#" class="font-weight-bold">[新規登録]</a>
    </div>
    <div>
        <table class="border">
            <tr class="border">
                <th>ID</th>
                <th>アプリ名</th>
                <th>画像</th>
                <th>シリアルコード</th>
                <th>状態</th>
                <th>編集</th>
            </tr>
            <?php for ($i = 1; $i < 5; $i++){ ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo "row 2" ?></td>
                <td><?php echo "row 3" ?></td>
                <td><?php echo "row 4" ?></td>
                <td><?php echo "row 5" ?></td>
                <td><a href=""><?php echo "row 6" ?></a></td>
            </tr>
            <?php } ?>
            <tr>
            </tr>
        </table>
    </div>
</div>

</body>
</html>
