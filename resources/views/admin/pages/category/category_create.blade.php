@extends('admin.layout.master')

@section('content')
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="d-flex">
                <h4 class="card-title">Thêm Loại Sản Phẩm </h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <button class="btn btn-secondary text-white mb-2"  onclick="location.href='../category'"> Trở Về </button>
                        </div>
                        <div class="col-lg-8 col-xlg-9 col-md-7">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal form-material mx-2" name="product_type" onsubmit="return validateForm()"  method="post" action="../category/create" >
                                            @csrf
                                        <div class="form-group">
                                            <label class="col-sm-12">Danh Mục Sản Phẩm Cha</label>
                                            <div class="col-sm-12">
                                                <select name="dad_id"
                                                        class="form-select shadow-none form-control-line">
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Danh Mục Sản Phẩm Con</label>
                                            <div class="col-md-12">
                                                <input type="text" name="name" placeholder="Nhập Danh Mục Con..." class="form-control form-control-line">
                                            </div>
                                            <p id="errror" class="d-none " style="color: red">Chưa Nhập Tên Loại Sản Phẩm !</p>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-success text-white">
                                                    Cập Nhật
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function validateForm() {
            let x = document.forms["product_type"]["name"].value;
            if (x === "") {
                var element = document.getElementById("errror");
                element.classList.remove("d-none")
                return false;
            }
        }
    </script>
@endsection
