@extends('admin.layout.master')

@section('content')
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="d-flex">
                <h4 class="card-title">Loại Sản Phẩm </h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <button class="btn btn-success text-white mb-2" onclick="location.href='category/create'">+ Tạo Mới </button>
                        </div>
                        <h4 class="card-title">Danh Sách : </h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">NO</th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Danh Mục Sản Phẩm Con</th>
                                    <th scope="col">Xóa Bỏ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($categories as $key => $type)
                                    <tr>
                                        <th scope="row">{{ ++$key }}</th>
                                        <td>{{ $type['id'] }}</td>
                                        <td>{{ $type['name'] }}</td>
                                        <td><button data-toggle="modal" data-target="#exampleModal"  class="btn btn-danger text-white" >Xóa</button><button onclick="location.href='product_type/cancel/{{ $type['id']}}'" id="close" class="d-none" ></button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header p-0" style="border: none">
                                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                                        <button type="button" class="close p-4" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body font-weight-bold m-auto">
                                        Bạn Có Thực Sự Muốn Xóa ?
                                    </div>
                                    <div class="m-auto pb-4 w-75 d-flex justify-content-center" style="border: none">
                                        <button type="button" class="btn btn-secondary mr-2" style="width: 100px" data-dismiss="modal">Không</button>
                                        <button type="button" id="close-confirm" class="btn btn-primary "  style="width: 100px">Xóa</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#close-confirm').click(function () {
            $('#close').click();
        })
    </script>
@endsection
