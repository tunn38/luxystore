@extends('admin.layout.master')

@section('content')
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="d-flex">
                <h4 class="card-title"> Thông Tin Khách Hàng </h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <button class="btn btn-success text-white mb-2" onclick="location.href='product/create'">+ Tạo Mới </button>
                        </div>
                        <h4 class="card-title">Danh Sách : </h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">NO</th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Tên Khách Hàng</th>
                                    <th scope="col">Tổng Tiền Đã Dùng</th>
                                    <th scope="col">Điểm Của Khách Hàng</th>
                                    <th scope="col">Vô Hiệu Hóa Tài Khoản</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($members as $key => $member)
                                    <tr>
                                        <th scope="row">{{ ++$key }}</th>
                                        <td>{{ $member['id'] }}</td>
                                        <td>{{ $member['name'] }}</td>
                                        <td>{{ $member['price'] }}</td>
                                        <td>{{ $member['sum'] }}</td>
                                        <td><button class="btn btn-danger text-white" onclick="location.href='product/delete/{{ $member['id']}}'">Vô Hiệu Hóa</button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
