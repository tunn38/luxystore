<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{ asset('css/admin/style.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{ asset('js/jquery-2.1.1.min.js') }}"></script>
    <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

    {{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--}}
    {{--    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>--}}

</head>
<body>
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
    <div
        id="main-wrapper"
        data-layout="vertical"
        data-navbarbg="skin5"
        data-sidebartype="full"
        data-sidebar-position="absolute"
        data-header-position="absolute"
        data-boxed-layout="full"
    >
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="#">
                        <!-- Logo icon -->
                        <b class="logo-icon">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img
                                src="https://demos.wrappixel.com/free-admin-templates/bootstrap/xtreme-bootstrap-free/assets/images/logo-icon.png"
                                alt="homepage"
                                class="dark-logo"
                            />
                            <!-- Light Logo icon -->
                            <img
                                src="https://demos.wrappixel.com/free-admin-templates/bootstrap/xtreme-bootstrap-free/assets/images/logo-light-icon.png"
                                alt="homepage"
                                class="light-logo"
                            />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                <!-- dark Logo text -->
                <img
                    src="https://demos.wrappixel.com/free-admin-templates/bootstrap/xtreme-bootstrap-free/assets/images/logo-text.png"
                    alt="homepage"
                    class="dark-logo"
                />
                            <!-- Light Logo text -->
                <img
                    src="https://demos.wrappixel.com/free-admin-templates/bootstrap/xtreme-bootstrap-free/assets/images/logo-light-text.png"
                    class="light-logo"
                    alt="homepage"
                />
              </span>
                    </a>
                    <a
                        class="nav-toggler waves-effect waves-light d-block d-md-none"
                        href="javascript:void(0)"
                    ><i class="ti-menu ti-close"></i
                        ></a>
                </div>
                <div
                    class="navbar-collapse collapse"
                    id="navbarSupportedContent"
                    data-navbarbg="skin5"
                >
                    <ul class="navbar-nav float-start me-auto">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box">
                            <a
                                class="nav-link waves-effect waves-dark"
                                href="javascript:void(0)"
                            ><i class="mdi mdi-magnify fs-4"></i
                                ></a>
                            <form class="app-search position-absolute">
                                <input
                                    type="text"
                                    class="form-control"
                                    placeholder="Search &amp; enter"
                                />
                                <a class="srh-btn"><i class="mdi mdi-close"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav float-end">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a
                                class="
                    nav-link
                    dropdown-toggle
                    text-muted
                    waves-effect waves-dark
                    pro-pic
                  "
                                href="#"
                                id="navbarDropdown"
                                role="button"
                                data-bs-toggle="dropdown"
                                aria-expanded="false"
                            >
                                <img
                                    src="https://demos.wrappixel.com/free-admin-templates/bootstrap/xtreme-bootstrap-free/assets/images/users/1.jpg"
                                    alt="user"
                                    class="rounded-circle"
                                    width="31"
                                />
                            </a>
                            <ul
                                class="dropdown-menu dropdown-menu-end user-dd animated"
                                aria-labelledby="navbarDropdown"
                            >
                                <a class="dropdown-item" href="javascript:void(0)"
                                ><i class="mdi mdi-account m-r-5 m-l-5"></i> My Profile</a
                                >
                                <a class="dropdown-item" href="javascript:void(0)"
                                ><i class="mdi mdi-wallet m-r-5 m-l-5"></i> My Balance</a
                                >
                                <a class="dropdown-item" href="javascript:void(0)"
                                ><i class="mdi mdi-email m-r-5 m-l-5"></i> Inbox</a
                                >
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                               href="{{ route('admin_index') }}"
                               aria-expanded="false"><i class="mdi mdi-view-dashboard"></i
                                ><span class="hide-menu">Trang Chủ</span></a
                            >
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                               href="{{ route('admin_category_dad') }}"
                               aria-expanded="false"><i class="mdi mdi-view-dashboard"></i
                                ><span class="hide-menu">Danh Mục Cha</span></a
                            >
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                               href="{{ route('admin_category') }}"
                               aria-expanded="false"><i class="mdi mdi-view-dashboard"></i
                                ><span class="hide-menu">Danh Mục Con</span></a
                            >
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                               href="{{ route('admin_product_type') }}"
                               aria-expanded="false"><i class="mdi mdi-view-dashboard"></i
                                ><span class="hide-menu">Loại Sản Phẩm</span></a
                            >
                        </li>
                        <!-- User Profile-->
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                               href="{{ route('admin_product') }}"
                               aria-expanded="false"><i class="mdi mdi-view-dashboard"></i
                                ><span class="hide-menu">Sản Phẩm</span></a
                            >
                        </li>
                        <li class="sidebar-item">
                            <a
                                class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="#"
                                aria-expanded="false"
                            ><i class="mdi mdi-account-network"></i
                                ><span class="hide-menu">Khuyến Mãi</span></a
                            >
                        </li>
                        <li class="sidebar-item">
                            <a
                                class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="#"
                                aria-expanded="false"
                            ><i class="mdi mdi-border-all"></i
                                ><span class="hide-menu">Hóa Đơn</span></a
                            >
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    @yield('header')
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
