<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\Admin\AdminIndexController;
use App\Http\Controllers\Clien\ClienInHomeController;
use App\Http\Controllers\Clien\ClienInProductsController;
use App\Http\Controllers\Clien\ClienInLoginController;
use App\Http\Controllers\Clien\ClienInCartController;
use App\Http\Controllers\Clien\ClienIntroduceController;
use App\Http\Controllers\Admin\AdminProductController;
use App\Http\Controllers\Admin\AdminProductTypeController;
use App\Http\Controllers\Admin\AdminCategoryController;
use App\Http\Controllers\Admin\AdminCategoryDadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/demo',function () {
    return view("home");
});
Route::get('/', [ClienInHomeController::class, 'index']);
Route::prefix('/')->group(function () {
    Route::get('/home', [ClienInHomeController::class, 'index']);
    Route::get('/cart', [ClienInCartController::class, 'index']);
    Route::get('/products', [ClienInProductsController::class, 'index']);
    Route::get('/introduce', [ClienIntroduceController::class, 'index']);
    Route::get('/login', [ClienInLoginController::class, 'index']);

});

Route::prefix('admin')->group(function () {
    Route::get('/', [AdminProductController::class, 'index'])->name('admin_index');

    Route::get('/product', [AdminProductController::class, 'index'])->name('admin_product');
    Route::match(['get', 'post'],'/product/create', [AdminProductController::class, 'create'])->name('create_product');
    Route::get('/product/cancel/{id}', [AdminProductController::class, 'cancel'])->name('cancel_product');

    Route::get('/product_type', [AdminProductTypeController::class, 'index'])->name('admin_product_type');
    Route::match(['get', 'post'],'/product_type/create', [AdminProductTypeController::class, 'create'])->name('create_product_type');
    Route::get('/product_type/cancel/{id}', [AdminProductTypeController::class, 'cancel'])->name('cancel_product_type');

    Route::get('/category', [AdminCategoryController::class, 'index'])->name('admin_category');
    Route::match(['get', 'post'],'/category/create', [AdminCategoryController::class, 'create'])->name('create_category');
    Route::get('/category/cancel/{id}', [AdminCategoryController::class, 'cancel'])->name('cancel_category');

    Route::get('/category_dad', [AdminCategoryDadController::class, 'index'])->name('admin_category_dad');
    Route::match(['get', 'post'],'/category_dad/create', [AdminCategoryDadController::class, 'create'])->name('create_category_dad');
    Route::get('/category_dad/cancel/{id}', [AdminCategoryDadController::class, 'cancel'])->name('cancel_category_dad');

    Route::get('/cancel/{id}', [AdminIndexController::class, 'cancel'])->name('admin_cancel_member');

});
