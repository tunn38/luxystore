<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryDad extends Model
{
    use HasFactory;
    protected $table = "category_dad";
    protected $fillable = [
        'id',
        'name',
        'status',
        'created_at',
        'updated_at',
    ];

}
