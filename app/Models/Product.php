<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'product_type_id',
        'name',
        'price',
        'description',
        'image_url',
        'quantity',
        'status',
        'created_at',
        'updated_at',
    ];

}
