<?php

namespace App\Http\Controllers\Clien;

use App\Models\Product;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClienInProductsController extends Controller
{

    public function index(){
        $new_products = Product::where('status','1')->orderBy('created_at','desc')->get()->take(10);
        $selling_products = Product::where('status','1')->get()->take(5);
        return view("clien.pages.products")->with(compact('new_products','selling_products'));
    }
}
