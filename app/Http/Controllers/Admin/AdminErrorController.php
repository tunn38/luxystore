<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\Category;
use App\Models\ProductType;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminErrorController extends Controller
{

    public function index()
    {
        $categories = Categories::where('status',1)->get()->all();
        return view("admin.pages.category.category_list")->with(compact('categories'));
    }

    public function create(Request $request)
    {
        if (!empty($_POST)) {
            $name = $request->input('name');
            $category = new  Categories();
            $category->name = $name;
            $category->save();
        }

        return view("admin.pages.category.category_create");
    }

    public function cancel($id)
    {
        $category = Categories::where('id',$id)->first();
        if($category) {
            $category->status = '0';
            $category->save();
            return redirect('/admin/categories');
        }
    }
}
