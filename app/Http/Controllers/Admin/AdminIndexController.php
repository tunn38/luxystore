<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminIndexController extends Controller
{
    public function index(){
    }

    public function cancel(){
        return view("admin.pages.member.member_cancel") ;
    }
}
