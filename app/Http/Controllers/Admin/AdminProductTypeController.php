<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\Category;
use App\Models\ProductType;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminProductTypeController extends Controller
{

    public function index()
    {
        $product_type = ProductType::where('status',1)->get()->all();
        return view("admin.pages.productType.product_type_list")->with(compact('product_type'));
    }

    public function create(Request $request)
    {
        if (!empty($_POST)) {
            $name = $request->input('name');
            $categories_id = $request->input('categories_id');
            $product_type = new  ProductType();
            $product_type->name = $name;
            $product_type->categories_id = $categories_id;
            $product_type->save();
        }
        $categories = Categories::where('status','1')->get()->all();
        return view("admin.pages.productType.product_type_create")->with(compact('categories'));
    }

    public function cancel($id)
    {
        $product_type = ProductType::where('id',$id)->first();
        if($product_type) {
            $product_type->status = '0';
            $product_type->save();
            return redirect('/admin/product_type');
        }
    }
}
