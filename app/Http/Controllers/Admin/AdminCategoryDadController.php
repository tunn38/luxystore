<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\CategoryDad;
use App\Models\Category;
use App\Models\ProductType;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminCategoryDadController extends Controller
{

    public function index()
    {
        $category_dad_list = CategoryDad::where('status',1)->get()->all();
        return view("admin.pages.categoryDad.category_dad_list")->with(compact('category_dad_list'));
    }

    public function create(Request $request)
    {
        if (!empty($_POST)) {
            $name = $request->input('name');
            $category = new  CategoryDad();
            $category->name = $name;
            $category->save();
        }

        return view("admin.pages.categoryDad.category_dad_create");
    }

    public function cancel($id)
    {
        $category = CategoryDad::where('id',$id)->first();
        if($category) {
            $category->status = '0';
            $category->save();
            return redirect('/admin/category_dad');
        }
    }
}
