<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\Category;
use App\Models\CategoryDad;
use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminProductController extends Controller
{

    public function index()
    {
        $products = Product::where('status','1')->get()->all();
        return view("admin.pages.product.product_list")->with(compact('products'));
    }

    public function create(Request $request)
    {
        if (!empty($_POST)){
            // ensure the request has a file before we attempt anything else.
            if ($request->hasFile('image')) {
                // Save the file locally in the storage/public/ folder under a new folder named /product
                $request->image->store('public/products');
                // Store the record, using the new file hashname which will be it's new filename identity.
                $product = new Product([
                    "product_type_id" => $request->get('product_type_id'),
                    "name" => $request->get('name'),
                    "price" => $request->get('price'),
                    "description" => $request->get('description'),
                    "image_url" => $request->image->hashName()
                ]);
                $product->save(); // Finally, save the record.
            }
            return redirect('/admin/product');
        }

        $categories = Category::where('status','1')->get()->all();
        $category_dad = CategoryDad::where('status','1')->get()->all();
        $product_type = ProductType::where('status','1')->get()->all();

        return view("admin.pages.product.product_create")->with(compact('product_type','categories','category_dad'));
    }

    public function cancel($id)
    {
        $product_type = Product::where('id',$id)->first();
        if($product_type) {
            $product_type->status = '0';
            $product_type->save();
            return redirect('/admin/product');
        }
    }
}
