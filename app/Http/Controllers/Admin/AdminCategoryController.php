<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\CategoryDad;
use App\Models\ProductType;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminCategoryController extends Controller
{

    public function index()
    {
        $categories = Category::where('status',1)->get()->all();
        return view("admin.pages.category.category_list")->with(compact('categories'));
    }

    public function create(Request $request)
    {
        if (!empty($_POST)) {
            $name = $request->input('name');
            $dad_id= $request->input('dad_id');
            $product_type = new  Category();
            $product_type->name = $name;
            $product_type->dad_id = $dad_id;
            $product_type->save();
        }
        $categories = CategoryDad::where('status','1')->get()->all();
        return view("admin.pages.category.category_create")->with(compact('categories'));
    }

    public function cancel($id)
    {
        $product_type = Category::where('id',$id)->first();
        if($product_type) {
            $product_type->status = '0';
            $product_type->save();
            return redirect('/admin/product_type');
        }
    }
}
